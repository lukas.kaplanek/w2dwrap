#!/usr/bin/env python
from __future__ import division
import urllib
import json
import os
import requests

from flask import Flask
from flask import request
from flask import make_response


# Flask app should start in global layout
app = Flask(__name__)

@app.route('/webhook', methods=['POST'])
def webhook():
    req = request.get_json(silent=True, force=True)

    #print("Request:")
    #print(json.dumps(req, indent=4))

    res = processRequest(req)

    res = json.dumps(res, indent=4)
    # print(res)
    r = make_response(res)
    r.headers['Content-Type'] = 'application/json'
    return r


def processRequest(req):
    print req.get("result").get("action")
    if req.get("result").get("action") == "ticket.winquery":
        res=ticketWinQuery(req)
        return res
    elif req.get("result").get("action") == "draw.last" or req.get("result").get("action") == "draw.next":
        res=getLastDraw(req)
        return res
    else:
        return {}

def ticketWinQuery(req):
    baseurl = "https://www.win2day.at/external/winqueryservice.jsp?j_ticketid="
    ticketid = getTicketid(req)
    if ticketid is None:
        return {}
    yql_url = baseurl + ticketid
    result = urllib.urlopen(yql_url).read()
    data = json.loads(result)
    res = makeWinqueryResult(data)
    return res

def getLastDraw(req):
    baseurl = "https://www.win2day.at/html/lotteryDataService"
    data = {"LotteryDataServiceRequest":{ "appId":"w2dPartner",  "requestedGames":["Lotto"],"numberOfDraws":2}	}
    headers={"Content-Type":"application/json"}
    result = requests.post(baseurl,json=data)
    resdata = result.json()
    res = makeLastDrawResult(resdata)
    return res


def getTicketid(req):
    result = req.get("result")
    parameters = result.get("parameters")
    ticketid = parameters.get("ticketid")
    if ticketid is None:
        return None

    return ticketid

def makeLastDrawResult(data):
    print("Request:")
    print(json.dumps(data, indent=4))
    query = data.get('LotteryDataServiceResponse')
    #print(json.dumps(query, indent=4))
    speech = 'Folgende Ziehungen sind vorhanden'
    if query is None:
        return {}
    for game in query.get('Games'):
        speech += " Game: " + game.get('name')
        for draw in game.get('Draw'):
            if draw.get('DrawResult') is None:
                speech += ' naechste Ziehung '+ str(draw.get('number'))+' am ' +draw.get('drawTime')[:10]
            else:
                speech += ' letzte Ziehung ' + str(draw.get('number'))+' am ' +draw.get('drawTime')[:10]
                speech += ' mit den Zahlen '
                numbers = '('
                for number in draw.get('DrawResult').get('Number'):
                    numbers += number.get('value')+','
                speech += numbers[:len(numbers)-1]+')'
                prizes = draw.get('PrizeTier').get('Tier')
                for prize in prizes:
                    if prize.get('rankNumber')==1:
                        speech += ' und es haben ' + str(prize.get('numberOfWins')) + ' Personen einen Sechser gewonnen'



    print("Response:")
    print(speech)

    return {
        "speech": speech,
        "displayText": speech,
        # "data": data,
        # "contextOut": [],
        "source": "dukesofdata-w2dwrap"
    }

def makeWinqueryResult(data):
    print("Request:")
    print(json.dumps(data, indent=4))
    query = data.get('winqueryTicket')
    #print(json.dumps(query, indent=4))
    if query is None:
        return {}

    ticketid = query.get('ticketId')
    #print(json.dumps(item, indent=4))
    if ticketid is None:
        return {}

    draws =query.get('draws')
    if draws is None:
        return {}
    drawItem =draws.get('draw')
    if drawItem is None:
        return {}

    winnings = query.get('winnings')
    print(json.dumps(winnings, indent=4))
    if winnings is not None:
        winamount = winnings.get('totalPrize')
        print(json.dumps(winamount, indent=4))
        if winamount is not None:
            winresponse = ' Gratulation sie haben '  + "{:.2f}".format(winamount/100) + ' gewonnen'
        else:
            winresponse = "Leider nicht gewonnen"
    else:
        winresponse = "Leider nicht gewonnen"

    #item = channel.get('item')
    #location = channel.get('location')
    #units = channel.get('units')
    #if (location is None) or (item is None) or (units is None):
    #    return {}

    #condition = item.get('condition')
    #if condition is None:
    #    return {}

    #print(json.dumps(item, indent=4))
    if(len(drawItem) > 1):
        speech = "Die letzten Ziehungen waren "
    else:
        speech = "Die letzte Ziehung war "

    for draw in drawItem:
        speech += draw.get('gameDescription') + " am " + draw.get('drawDate')[:10] + " "

    speech = speech + ". " + winresponse
    print("Response:")
    print(speech)

    return {
        "speech": speech,
        "displayText": speech,
        # "data": data,
        # "contextOut": [],
        "source": "dukesofdata-w2dwrap"
    }


if __name__ == '__main__':
    port = int(os.getenv('PORT', 5000))

    print "Starting app on port %d" % port

    app.run(debug=False, port=port, host='0.0.0.0')
